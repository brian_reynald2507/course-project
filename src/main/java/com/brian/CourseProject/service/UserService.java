package com.brian.CourseProject.service;

import com.brian.CourseProject.model.io.UserResponseData;

public interface UserService {
	public UserResponseData[] getTraineeByCourse(int id);
}
