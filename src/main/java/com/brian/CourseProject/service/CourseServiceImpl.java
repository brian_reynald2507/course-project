package com.brian.CourseProject.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brian.CourseProject.model.Course;
import com.brian.CourseProject.model.io.CourseOutput;
import com.brian.CourseProject.model.io.UserResponse;
import com.brian.CourseProject.repo.CourseRepo;
import com.brian.CourseProject.repo.UserRepo;

@Service
public class CourseServiceImpl implements CourseService {
	@Autowired
	CourseRepo courseRepo;

	@Autowired
	UserRepo userRepo;
	
	public ArrayList<CourseOutput> GetCourse() {
		Integer[] id = new Integer[1];
		ArrayList<CourseOutput> result = new ArrayList<>();
		
		UserResponse userResponse = new UserResponse();
		Iterable<Course> courses = courseRepo.findAll();
		
		for (Course c : courses) {
			CourseOutput temp = new CourseOutput();
			temp.setIdCourse(c.getIdCourse());
			temp.setLocation(c.getLocation());
			temp.setSubject(c.getSubject());
			temp.setCourseDate(c.getCourseDate());
			temp.setIdTrainer(c.getIdTrainer());
			
			id[0] = c.getIdTrainer();
			userResponse = userRepo.GetUser(id);
			if (userResponse.getData() != null) {
				temp.setTrainerName(userResponse.getData()[0].getNama());
			} else {
				temp.setTrainerName(null);
			}
			result.add(temp);
		}
		
		return result;
	}
	
	public CourseOutput GetCourseById(int id) {
//		Course c = new Course();
//		c = courseRepo.findByTrainer(id);
//		
//		CourseOutput courseOutput = new CourseOutput();
//		
//		UserResponse userResponse = new UserResponse();
//		Integer[] id_temp = new Integer[1];
//		if (c!=null) {
//
//			courseOutput.setIdCourse(c.getIdCourse());
//			courseOutput.setCourseDate(c.getCourseDate());
//			courseOutput.setSubject(c.getSubject());
//			courseOutput.setLocation(c.getLocation());
//			courseOutput.setIdTrainer(c.getIdTrainer());
//			
//			id_temp[0]=c.getIdTrainer();
//			userResponse = userRepo.GetUser(id_temp);
//			
//			if(userResponse.getData()!=null) {
//				courseOutput.setTrainerName(userResponse.getData()[0].getNama());
//			} else {
//				courseOutput.setTrainerName(null);
//			}
//			
//			return courseOutput;
//		}
		
		
		Iterable<Course> courses = courseRepo.findAll();
		CourseOutput courseOutput = new CourseOutput();
		
		UserResponse userResponse = new UserResponse();
		Integer[] id_temp = new Integer[1];
				
		for (Course c : courses) {
			if (c.getIdCourse()==id) {
				courseOutput.setIdCourse(c.getIdCourse());
				courseOutput.setCourseDate(c.getCourseDate());
				courseOutput.setSubject(c.getSubject());
				courseOutput.setLocation(c.getLocation());
				courseOutput.setIdTrainer(c.getIdTrainer());
				
				id_temp[0]=c.getIdTrainer();
				userResponse = userRepo.GetUser(id_temp);
				
				if(userResponse.getData()!=null) {
					courseOutput.setTrainerName(userResponse.getData()[0].getNama());
				} else {
					courseOutput.setTrainerName(null);
				}
				
				return courseOutput;
			}
		}
		
		return null;
	}
	
	public List<Course> getCourseByLocation(String location) {
		List<Course> c = new ArrayList<Course>();
		c = courseRepo.findByLocation(location);
		return c;
	}

	public Course GetCourseById2(int id) {
		Course c = new Course();
		c = courseRepo.findByIdTrainer(id);
		return c;
	}
	
	public Course GetCourseBySubject(String subject) {
		Course c = new Course();
		c = courseRepo.findBySubject(subject);
		return c;
	}
}
