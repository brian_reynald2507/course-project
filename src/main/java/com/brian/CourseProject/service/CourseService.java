package com.brian.CourseProject.service;

import java.util.ArrayList;
import java.util.List;

import com.brian.CourseProject.model.Course;
import com.brian.CourseProject.model.io.CourseOutput;

public interface CourseService {
	public ArrayList<CourseOutput> GetCourse();
	public List<Course> getCourseByLocation(String location);
	public CourseOutput GetCourseById(int id);
	public Course GetCourseById2(int id);
	public Course GetCourseBySubject(String subject);
}
