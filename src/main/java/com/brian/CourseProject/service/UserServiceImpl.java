package com.brian.CourseProject.service;

import java.util.ArrayList;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brian.CourseProject.model.Trainee;
import com.brian.CourseProject.model.io.UserResponseData;
import com.brian.CourseProject.repo.CourseRepo;
import com.brian.CourseProject.repo.TraineeRepo;
import com.brian.CourseProject.repo.UserRepo;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepo userRepo;
	
	@Autowired
	CourseRepo courseRepo;
	
	@Autowired
	TraineeRepo traineeRepo;
	
	public UserResponseData[] getTraineeByCourse(int id){
		Iterable<Trainee> trainees = traineeRepo.findAll();
		Integer[] idTrainee = null;
		
		ArrayList<Integer> arrInt = new ArrayList<>();
		
		for (Trainee t : trainees) {
			if (t.getIdCourse()==id) {
				arrInt.add(t.getIdTrainee());
			}
		}
		
		idTrainee = arrInt.toArray(new Integer[arrInt.size()]);
		
		return userRepo.GetUser(idTrainee).getData();
	}
}
