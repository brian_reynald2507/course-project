package com.brian.CourseProject.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.brian.CourseProject.model.Trainee;

@Repository
public interface TraineeRepo extends CrudRepository<Trainee, Integer> {

}
