package com.brian.CourseProject.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.brian.CourseProject.model.Course;

@Repository
public interface CourseRepo extends CrudRepository<Course, Integer> {
	Course findByIdTrainer(int idTrainer);
	List<Course> findByLocation(String location);
	Course findBySubject(String subject);
}
