package com.brian.CourseProject.repo;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.brian.CourseProject.constant.CourseProjectConstant;
import com.brian.CourseProject.model.io.UserRequest;
import com.brian.CourseProject.model.io.UserResponse;

@Repository
public class UserRepo{
	public UserResponse GetUser(Integer[] id) {
		RestTemplate restTemplate = new RestTemplate();
		
		String url = CourseProjectConstant.HOST_USER+"/user/get-by-id";
		
		UserRequest userRequest = new UserRequest();
		userRequest.setId(id);
		
		ResponseEntity<UserResponse> userResponse = null;
		try {
			userResponse = restTemplate.postForEntity(url, userRequest, UserResponse.class);
			
			return userResponse.getBody();
			
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		} finally {
		
		}
	}
}
