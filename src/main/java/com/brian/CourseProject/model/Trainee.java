package com.brian.CourseProject.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Trainee {
	@Id @GeneratedValue(strategy = GenerationType.AUTO, generator = "increment")
	@GenericGenerator(strategy = "increment", name ="increment")
	private int idTrainee;
	private int idCourse;
	
	public Trainee() {
	
	}
	
	public Trainee(int idCourse) {
		this.idCourse = idCourse;
	}
	
	public int getIdCourse() {
		return idCourse;
	}
	public void setIdCourse(int idCourse) {
		this.idCourse = idCourse;
	}
	public int getIdTrainee() {
		return idTrainee;
	}
}
