package com.brian.CourseProject.model.io;

import java.sql.Date;

public class CourseOutput {
	private int idCourse;
	private String subject;
	private String location;
	private int idTrainer;
	private String trainerName;
	private Date courseDate;
	
	public CourseOutput() {
	}
	
	
	public int getIdTrainer() {
		return idTrainer;
	}

	public void setIdTrainer(int idTrainer) {
		this.idTrainer = idTrainer;
	}
	public int getIdCourse() {
		return idCourse;
	}
	public void setIdCourse(int idCourse) {
		this.idCourse = idCourse;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTrainerName() {
		return trainerName;
	}
	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}
	public Date getCourseDate() {
		return courseDate;
	}
	public void setCourseDate(Date courseDate) {
		this.courseDate = courseDate;
	}
}
