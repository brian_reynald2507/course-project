package com.brian.CourseProject.model.io;

public class UserResponse {
	private UserResponseData[] data;

	public UserResponseData[] getData() {
		return data;
	}

	public void setData(UserResponseData[] data) {
		this.data = data;
	}
}
