package com.brian.CourseProject.model;

public class Output {
	private Object data;

	public Output(Object data) {
		this.data = data;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
}
