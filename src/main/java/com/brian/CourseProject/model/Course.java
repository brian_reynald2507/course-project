package com.brian.CourseProject.model;

import java.sql.Date;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="course")
public class Course {
	@Id @GeneratedValue(strategy=GenerationType.AUTO, generator ="increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	@Column(name="id_course")
	private int idCourse;
	
	@Column(name="subject")
	private String subject;
	
	@Column(name="location")
	private String location;
	
	@Column(name="id_trainer")
	private int idTrainer;
	
	@Column(name="course_date")
	private Date courseDate;
	@JsonIgnore
	@Column(name="last_edited")
	private Timestamp lastEdited;
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getIdTrainer() {
		return idTrainer;
	}
	public void setIdTrainer(int idTrainer) {
		this.idTrainer = idTrainer;
	}
	public Date getCourseDate() {
		return courseDate;
	}
	public void setCourseDate(Date courseDate) {
		this.courseDate = courseDate;
	}
	public Timestamp getLastEdited() {
		return lastEdited;
	}
	public void setLastEdited(Timestamp lastEdited) {
		this.lastEdited = lastEdited;
	}
	public int getIdCourse() {
		return idCourse;
	}
	
}
