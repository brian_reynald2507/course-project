package com.brian.CourseProject.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.brian.CourseProject.model.Course;
import com.brian.CourseProject.model.Output;
import com.brian.CourseProject.model.io.CourseOutput;
import com.brian.CourseProject.model.io.GetCourseByIDInput;
import com.brian.CourseProject.service.CourseService;

@RestController
@RequestMapping("/course")
public class CourseController {
	@Autowired
	CourseService courseService;
	
//	@GetMapping(value = "/", produces="application/json")
//	public @ResponseBody ArrayList<Course> getCourse(){
//		ArrayList<Course> temp = new ArrayList<>();
//		temp = courseService.GetCourse();
//		return temp;
//	}
	
	@GetMapping(value = "/", produces="application/json")
	public @ResponseBody Output getCourse(){
		ArrayList<CourseOutput> temp = new ArrayList<>();
		temp = courseService.GetCourse();
		return new Output(temp);
	}
	
//	@PostMapping(value="/by-id", consumes = "application/json", produces="application/json")
//	public @ResponseBody Course getCourseById(@RequestBody GetCourseByIDInput input) {
//		Course result = new Course();
//		result = courseService.GetCourseById(input.getId());
//		return result;
//	}
	
	@PostMapping(value="/by-id", consumes = "application/json", produces="application/json")
	public @ResponseBody Output getCourseById(@RequestBody GetCourseByIDInput input) {
		CourseOutput result = new CourseOutput();
		result = courseService.GetCourseById(input.getId());
		return new Output(result);
	}
	
	@PostMapping(value="/by-location", consumes = "application/json", produces="application/json")
	public @ResponseBody List<Course> getCourseByLocation(@RequestBody GetCourseByIDInput input) {
		List<Course> result = new ArrayList<Course>();
		result = courseService.getCourseByLocation(input.getLocation());
		return result;
	}
	
	@PostMapping(value="/by-id2", consumes = "application/json", produces="application/json")
	public @ResponseBody Output getCourseById2(@RequestBody GetCourseByIDInput input) {
		Course result = new Course();
		result = courseService.GetCourseById2(input.getId());
		return new Output(result);
	}
	
	@PostMapping(value="/by-subject", consumes = "application/json", produces="application/json")
	public @ResponseBody Output getCourseByCourse(@RequestBody GetCourseByIDInput input) {
		Course result = new Course();
		result = courseService.GetCourseBySubject(input.getSubject());
		return new Output(result);
	}
}