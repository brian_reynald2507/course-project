package com.brian.CourseProject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.brian.CourseProject.model.Output;
import com.brian.CourseProject.model.io.UserInput;
import com.brian.CourseProject.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	UserService userService;
	
	@PostMapping(value = "/", consumes = "application/json", produces = "application/json")
	public @ResponseBody Output getTraineeByCourse(@RequestBody UserInput input) {
		return new Output(userService.getTraineeByCourse(input.getId()));
	}
}
